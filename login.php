<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>Private Chat</title>
    <style>
        * {
            box-sizing: border-box!important;
            text-align: center;
            font-size: 25px;
        }
        body {
            padding: 0;
            margin: 0;
            background-color: lightyellow;
        }
        form {
            max-width: 400px;
            margin: 0 auto;
            color: white;

        }


        input[type="text"] {
            width: 100%;
            text-align: left;
            padding: 10px;
            margin: 5px 0;
            border: 0;

        }

        input[type="submit"] {
            padding: 10px 20px;

            color: white;
            text-transform: uppercase;
            background-color: transparent;
            border: 1px solid white;

        }
        div{
            background-color: cadetblue;
        }
    </style>
</head>
<body>
<h2>Welcome to Messenger</h2>
<?php
session_start();

if(isset($_POST['userName'])) {
    $_SESSION['userName'] = $_POST['userName'];
}

if( !isset($_SESSION['userName']) )
{


?>
<div>
<form action="messenger.php" method="post">

    Enter your name:
    <input type="text" name="userName" placeholder="User Name">
    <input type="submit" >

</form></div>
    <?php
}
else{


    echo "Welcome ".$_SESSION['userName']."!";


}


?>


</body>
</html>